package com.example.notekeeper

// Does not need a primary constructor
// When we declare it as an object, it becomes a singleton class and available throughout the app
object DataManager {

    val courses = HashMap<String, CourseInfo>()

    val notes = ArrayList<NoteInfo>()

    // Initialiser block
    // Similar to a constructor in Java - it gets called after the class is created
    // They do NOT accept parameters but can access properties
    init {

        initializeCourses()
        initializeNotes()

    }

    private fun initializeNotes() {

        var course = CourseInfo("Java", "Integers")

        //courses.set(course.courseId, course)
        var note1 = NoteInfo(course, "title of note 1", "some text here 1")
        var note2 = NoteInfo(course, "title of note 2", "some text here 2")
        var note3 = NoteInfo(course, "title of note 3", "some text here 3")

        notes.add(note1)
        notes.add(note2)
        notes.add(note3)


    }

    private fun initializeCourses() {

        // Using positional parameters
        var course = CourseInfo("Java", "Integers")

        courses.set(course.courseId, course)

        // Alternative method: Using named parameters

        course = CourseInfo(courseId = "Android", title = "Intents")

        courses.set(course.courseId, course)


        // Alternative method: Using named parameters but different order

        course = CourseInfo(title = "Kotlin", courseId = "Classes")

        courses.set(course.courseId, course)


        course = CourseInfo("C#", "Collections")

        courses.set(course.courseId, course)

    }


}