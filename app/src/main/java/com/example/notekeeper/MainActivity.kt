package com.example.notekeeper

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // Set up a mutable property to indicate position of a note
    // Start with position not set
    private var notePosition = POSITION_NOT_SET

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        // This code determines what layout gets loaded
        // R is a generated class
        // Layout names are contained in R.layout
        // Kotlin generates synthetic properties, making it easier than Java
        // Property has same name as view ID
        setContentView(R.layout.activity_main)


        // ONce you create the object it needs to be removed
        // val dm  = DataManager()

        // Array is a generic type and we need to tell the type

        val adapterCourses = ArrayAdapter<CourseInfo>(
            this,
            android.R.layout.simple_spinner_item,
            DataManager.courses.values.toList()
        )

        adapterCourses.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinnerCourses.adapter = adapterCourses


        // If intent sends a position we grab it otherwise we get default -1
        notePosition = intent.getIntExtra(EXTRA_NOTE_POSITION, POSITION_NOT_SET)

        if (notePosition != POSITION_NOT_SET) {

            displayNote()
        }


    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            R.id.action_next -> {

                moveNext()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun moveNext() {

        ++notePosition // note, we are not checking end of list, will throw an error
        displayNote()
        // this call forces a change to the menu
        invalidateOptionsMenu() // Make this call so it triggers the call to onPrepareOptionsMenu

    }


    /*
    Displays a chosen note
    Grabs the note based on note position passed to the intent
    Sets the text properties of the text and title from the note properties
     */
    private fun displayNote() {

        // Retrieve the details of the note
        val note = DataManager.notes[notePosition] // note square brackets
        textNoteTitle.setText(note.title)
        textNoteText.setText(note.text)

        //And we need to set the spinner
        val coursePosition = DataManager.courses.values.indexOf(note.course)

        spinnerCourses.setSelection(coursePosition)


    }
//Press Ctrl O to bring up a list of functions that can be overridden
    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {

    // Check if we have reached end of notes
    if (notePosition >= DataManager.notes.lastIndex) {

        // Locate the menu we want to change(? means accessing in null safe way
        val menuItem = menu?.findItem(R.id.action_next)

        if (menuItem != null) {

            menuItem.icon = getDrawable(R.drawable.ic_block_white_24dp)
            menuItem.isEnabled = false // prevent them moving forward
        }

    }

        return super.onPrepareOptionsMenu(menu)
    }
}
