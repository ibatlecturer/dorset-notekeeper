package com.example.notekeeper

// Note val is for immutable properties (cannot be changed)
// var is for mutable properties (can be chaged
// Note tha Kotlin does all the work in the constructor (getter, setting assign)

data class CourseInfo(val courseId: String, val title: String) {
    override fun toString(): String {
        // return super.toString()

        return title
    }
}
// Since this has only properties we can make it a data class
// Kotlin now generates the internal methods for us

data class NoteInfo(var course: CourseInfo, var title: String, var text: String)

