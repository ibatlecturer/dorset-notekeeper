package com.example.notekeeper

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.ArrayAdapter
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_note_list_activty.*
import kotlinx.android.synthetic.main.content_note_list_activty.*

class NoteListActivty : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_list_activty)
        setSupportActionBar(toolbar)

        // Modify so you opens up a note editor
        fab.setOnClickListener { view ->
           // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            //    .setAction("Action", null).show()

            //We tell it is a class ::class but expecting a java class so we append .java
            val activityIntent = Intent(this,MainActivity::class.java)

            startActivity(activityIntent)

        }


        // Add the listNotes adapter to NoteListActivity
        listNotes.adapter = ArrayAdapter (
            this,
            android.R.layout.simple_expandable_list_item_1,
            DataManager.notes)


        // NoteListActivity.kt
        // Add code to listen out for a click on a note
        // Notice these are { } braces and NOT brackets

        listNotes.setOnItemClickListener{parent, view, position, id ->

            val activityIntent = Intent(this, MainActivity::class.java)
            activityIntent.putExtra(EXTRA_NOTE_POSITION, position)
            startActivity(activityIntent)
        }

    }




}
